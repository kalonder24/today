//
//  UIContentConfiguration+Stateless.swift
//  Today
//
//  Created by Kaloyan on 28.07.22.
//

import UIKit

extension UIContentConfiguration {
    func updated(for state: UIConfigurationState) -> Self {
        return self
    }
}
